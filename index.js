const { Client, Intents } = require('discord.js');
const { token } = require('./config.json');
const { SlashCommandBuilder } = require('@discordjs/builders');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });


client.once('ready', () => {
    client.user.setStatus("dnd");
    client.user.setActivity("https://pronouns.page");
    console.log("Logged in as " + client.user.tag);
});

client.login(token);


